import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:timer_bloc/simple_bloc_observer.dart';
import 'package:timer_bloc/timer/timer.dart';

import 'post/posts.dart';

void main(){
  Bloc.observer  = SimpleBlocObserver();
  runApp(const App());
}


class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Timer',
      theme: ThemeData(
        colorScheme: const ColorScheme.light(
          primary: Color.fromRGBO(72, 74, 126, 1),
        ),
      ),
      home: const PostPage(),
      // home: const TimerPage(),
    );
  }
}